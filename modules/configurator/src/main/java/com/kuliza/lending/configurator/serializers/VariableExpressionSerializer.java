package com.kuliza.lending.configurator.serializers;

import com.fasterxml.jackson.databind.util.StdConverter;
import com.kuliza.lending.configurator.CreditEngineApplication;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.VariableDao;
import com.kuliza.lending.configurator.utils.HelperFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VariableExpressionSerializer extends StdConverter<String, String> {

	private static final Logger logger = LoggerFactory.getLogger(CreditEngineApplication.class);

	@Autowired
	private VariableDao variableDao;

	@Autowired
	private GroupDao groupDao;

	@Override
	public String convert(String expression) {
		try {
			if (expression != null) {
				return HelperFunctions.makeReadableExpressionString(expression, variableDao, groupDao, true);
			} else {
				return "";
			}
		} catch (Exception e) {
			logger.error(HelperFunctions.getStackTrace(e));
			// Here we are setting expression string to empty
			return "";
		}
	}

}
