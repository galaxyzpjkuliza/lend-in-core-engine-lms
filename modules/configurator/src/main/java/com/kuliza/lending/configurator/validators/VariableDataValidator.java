package com.kuliza.lending.configurator.validators;

import com.kuliza.lending.configurator.models.Variable;
import com.kuliza.lending.configurator.models.VariableDao;
import com.kuliza.lending.configurator.pojo.NewVariable;
import com.kuliza.lending.configurator.pojo.SubmitNewVariables;
import com.kuliza.lending.configurator.pojo.UpdateVariable;
import com.kuliza.lending.configurator.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

@Component
public class VariableDataValidator extends BasicDataValidator {

	@Autowired
	private VariableDao variableDao;

	public boolean validateGetAllVariables(String userId, String productId) throws Exception {
		return !validateProductId(productId, userId) ? false : true;
	}

	public boolean validateGetSingleVariable(String userId, String productId, String variableId) throws Exception {
		if (!validateProductId(productId, userId)) {
			return false;
		}
		return !validateVariableId(variableId, productId) ? false : true;
	}

	public void checkDerivedVariables(String expressionString, String productId, Errors errors) throws Exception {
		if (expressionString.equals("")) {
			errors.rejectValue(Constants.EXPRESSION_STRING, Constants.INVALID_EXPRESSION_STRING,
					Constants.VARIABLE_EXPRESSION_REQUIRED_MESSAGE);
		} else {
			boolean checkError = validateExpressionString(expressionString, true, productId, errors);
			if (!checkError) {
				errors.rejectValue(Constants.EXPRESSION_STRING, Constants.INVALID_EXPRESSION_STRING,
						Constants.INVALID_EXPRESSION_MESSAGE);
			}
		}
	}

	public void validateVariable(NewVariable obj, Errors errors) throws Exception {
		if (errors.hasErrors()) {
			return;
		}
		if (variableDao.findByNameAndProductIdAndIsDeleted(obj.getVariableName(), Long.parseLong(obj.getProductId()),
				false) != null) {
			errors.rejectValue(Constants.VARIABLE_NAME, Constants.INVALID_VARIABLE_NAME,
					Constants.VARIABLE_NAME_TAKEN_MESSAGE);
		}

		if (obj.getVariableName().toUpperCase().matches(Constants.NOT_VARIABLE_NAMES_REGEX)) {
			errors.rejectValue(Constants.VARIABLE_NAME, Constants.INVALID_VARIABLE_NAME,
					Constants.INVALID_VARIABLE_NAME_MESSAGE);
		}

		if (obj.getVariableCategory().equals("Derived")) {
			checkDerivedVariables(obj.getExpressionString(), obj.getProductId(), errors);
		} else {
			obj.setExpressionString("");
		}
	}

	public void validateNewVariable(SubmitNewVariables obj, Errors errors) throws Exception {
		if (errors.hasErrors()) {
			return;
		}
		validateProductVariable(obj.getUserId(), obj.getProductId(), errors);
	}

	public void validateUpdateVariable(UpdateVariable obj, Errors errors) throws Exception {
		if (errors.hasErrors()) {
			return;
		}
		validateProductVariable(obj.getUserId(), obj.getProductId(), errors);
		if (!validateVariableId(obj.getVariableId(), obj.getProductId())) {
			errors.rejectValue(Constants.VARIABLE_ID, Constants.INVALID_VARIABLE_ID,
					Constants.INVALID_VARIABLE_ID_MESSAGE);
		} else {
			Variable variable = variableDao.findByIdAndIsDeleted(Long.parseLong(obj.getVariableId()), false);
			// TODO : Check Cyclic Condition For Expression String
			if (variable.getCategory().equals(Constants.DERIVED)) {
				checkDerivedVariables(obj.getExpressionString(), obj.getProductId(), errors);
			} else {
				obj.setExpressionString("");
			}
		}

	}

}
