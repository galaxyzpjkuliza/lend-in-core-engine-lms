package com.kuliza.lending.configurator.pojo;

import java.util.List;

public class GetRules {

	List<ResponseRuleDef> rules;
	String isActive;
	String outputWeight;
	String variableName;
	String variableType;
	String variableSource;

	public GetRules() {
		super();
	}

	public GetRules(List<ResponseRuleDef> rules, String isActive, String outputWeight, String variableName,
			String variableType, String variableSource) {
		super();
		this.rules = rules;
		this.isActive = isActive;
		this.outputWeight = outputWeight;
		this.variableName = variableName;
		this.variableType = variableType;
		this.variableSource = variableSource;
	}

	public List<ResponseRuleDef> getRules() {
		return rules;
	}

	public void setRules(List<ResponseRuleDef> rules) {
		this.rules = rules;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getOutputWeight() {
		return outputWeight;
	}

	public void setOutputWeight(String outputWeight) {
		this.outputWeight = outputWeight;
	}

	public String getVariableName() {
		return variableName;
	}

	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}

	public String getVariableType() {
		return variableType;
	}

	public void setVariableType(String variableType) {
		this.variableType = variableType;
	}

	public String getVariableSource() {
		return variableSource;
	}

	public void setVariableSource(String variableSource) {
		this.variableSource = variableSource;
	}

}
