package com.kuliza.lending.configurator.validators;

import com.kuliza.lending.configurator.models.UserDao;
import com.kuliza.lending.configurator.pojo.SubmitNewUser;
import com.kuliza.lending.configurator.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

@Component
public class UserDataValidator {

	@Autowired
	private UserDao userDao;

	public void validateUser(SubmitNewUser obj, Errors errors) throws Exception {
		if (errors.hasErrors()) {
			return;
		}
		if (userDao.findByUserName(obj.getUserName()) != null) {
			errors.rejectValue(Constants.USER_NAME, Constants.INVALID_USER_NAME, Constants.USERNAME_TAKEN_MESSAGE);
		}
	}

}
