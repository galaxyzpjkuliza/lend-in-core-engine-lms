package com.kuliza.lending.configurator.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.SubmitNewGroup;
import com.kuliza.lending.configurator.pojo.UpdateGroup;
import com.kuliza.lending.configurator.service.groupservice.GroupServices;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.validators.GroupDataValidator;

@RestController
@RequestMapping(value = "/api/product/{productId:^[1-9]+[0-9]*$}/group")
public class GroupControllers {

	private static final Logger logger = LoggerFactory.getLogger(GroupControllers.class);

	@Autowired
	private GroupServices groupServices;

	@Autowired
	private GroupDataValidator groupDataValidator;

	// API to get list of groups inside a single Product
	@RequestMapping(method = RequestMethod.GET, value = "/list")
	public Object getAllGroupsList(@PathVariable(value = "productId") String productId, HttpServletRequest request) {
		logger.info("--> Entering get all group List()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			if (!groupDataValidator.validateGetAllGroup(userId, productId)) {
				logger.error("Invalid productId: " + productId);
				response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
						Constants.INVALID_PRODUCT_ID_MESSAGE);
			} else {
				logger.debug("getting list of groups inside a single Product:: productId: " + productId);
				response = groupServices.getGroupsList(productId);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfuly fetched group List");
				groupServices.logSuccessResponse(request, userId, response);
			} else {
				groupServices.logErrorResponse(request, userId, response);
			}
			logger.info("<-- Exiting get all group List()");
			return response;
		} catch (Exception e) {
			return groupServices.handleException(e, request, userId);
		}
	}

	// API to get all Groups data inside a Product
	@RequestMapping(method = RequestMethod.GET, value = "")
	public Object getAllGroupsData(@PathVariable(value = "productId") String productId, HttpServletRequest request) {
		logger.info("-->Entering get all group data()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			if (!groupDataValidator.validateGetAllGroup(userId, productId)) {
				logger.error("Error Invalid productId: " + productId);
				response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
						Constants.INVALID_PRODUCT_ID_MESSAGE);
			} else {
				logger.debug("getting all Groups data inside a Product with Id: " + productId);
				response = groupServices.getGroupsData(productId);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully fetched group data");
				groupServices.logSuccessResponse(request, userId, response);
			} else {
				groupServices.logErrorResponse(request, userId, response);
			}
			logger.info("<--Exiting get all group data()");
			return response;
		} catch (Exception e) {
			return groupServices.handleException(e, request, userId);
		}
	}

	// API to get single groups data inside a Product
	@RequestMapping(method = RequestMethod.GET, value = "/{groupId:^[1-9]+[0-9]*$}")
	public Object getGroup(@PathVariable(value = "productId") String productId,
			@PathVariable(value = "groupId") String groupId, HttpServletRequest request) {
		logger.info("--> Entering get Group()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			if (!groupDataValidator.validateGetSingleGroup(userId, productId, groupId)) {
				logger.error(Constants.INVALID_PRODUCT_GROUP_ID_MESSAGE + ":: productId:" + productId + "::groupId:"
						+ groupId);
				response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
						Constants.INVALID_PRODUCT_GROUP_ID_MESSAGE);
			} else {
				logger.debug("getting single group data inside a product");
				response = groupServices.getGroupData(groupId);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully Fetched group data");
				groupServices.logSuccessResponse(request, userId, response);
			} else {
				groupServices.logErrorResponse(request, userId, response);
			}
			logger.info("<-- Exiting get Group()");
			return response;
		} catch (Exception e) {
			return groupServices.handleException(e, request, userId);
		}
	}

	// API to save new Group inside a product
	@RequestMapping(method = RequestMethod.POST, value = "")
	public Object groupSubmit(@PathVariable(value = "productId") String productId,
			@Valid @RequestBody SubmitNewGroup input, BindingResult result, HttpServletRequest request) {
		logger.info("--> Entering group Submit()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			input.setUserId(userId);
			input.setProductId(productId);
			groupDataValidator.validateNewGroup(input, result);
			response = groupServices.checkErrors(result);
			if (response == null) {
				logger.debug("creating new group Inside a product with name: " + input.getGroupName());
				response = groupServices.createNewGroup(input);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully created group with name: " + input.getGroupName());
				groupServices.logSuccessResponse(request, userId, response, input);
			} else {
				groupServices.logErrorResponse(request, userId, response, input);
			}
			logger.info("<-- Exiting group Submit()");
			return response;
		} catch (Exception e) {
			return groupServices.handleException(e, request, userId, input);
		}
	}

	// API to update Group Name
	@RequestMapping(method = RequestMethod.PUT, value = "/{groupId:^[1-9]+[0-9]*$}")
	public Object updateGroup(@PathVariable(value = "productId") String productId,
			@PathVariable(value = "groupId") String groupId, @Valid @RequestBody UpdateGroup input,
			BindingResult result, HttpServletRequest request) {
		logger.info("--> Entering update Group()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			input.setUserId(userId);
			input.setProductId(productId);
			input.setGroupId(groupId);
			groupDataValidator.validateUpdateGroup(input, result);
			response = groupServices.checkErrors(result);
			if (response == null) {
				logger.debug("updating group name to:" + input.getGroupName());
				response = groupServices.updateGroup(input);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully updated Group Name");
				groupServices.logSuccessResponse(request, userId, response, input);
			} else {
				groupServices.logErrorResponse(request, userId, response, input);
			}
			logger.info("<--Exiting update Group()");
			return response;
		} catch (Exception e) {
			return groupServices.handleException(e, request, userId, input);
		}
	}

}
