package com.kuliza.lending.configurator.drools;

import com.kuliza.lending.configurator.pojo.KieContainerBean;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DroolsEngine {

	@Autowired
	private KieContainerBean kieContainerBean;

	public <T> void getScore(T droolsInput, String filename, String environment, String containerId) throws Exception {
		KieSession kieSession = kieContainerBean.getSingleContainer(environment, containerId)
				.newKieSession("session_" + filename.split("\\.")[0]);
		kieSession.insert(droolsInput);
		kieSession.fireAllRules();
		kieSession.dispose();
	}

}
