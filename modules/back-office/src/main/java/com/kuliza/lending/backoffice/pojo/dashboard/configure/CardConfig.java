package com.kuliza.lending.backoffice.pojo.dashboard.configure;

import java.util.List;
import java.util.Set;

public class CardConfig {

	private String label;
	private String key;
	private Set<String> variables;
	private List<HeaderConfig> headers;

	public CardConfig() {
		super();
	}

	public CardConfig(String label, String key, Set<String> variables, List<HeaderConfig> headers) {
		super();
		this.label = label;
		this.key = key;
		this.variables = variables;
		this.headers = headers;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Set<String> getVariables() {
		return variables;
	}

	public void setVariables(Set<String> variables) {
		this.variables = variables;
	}

	public List<HeaderConfig> getHeaders() {
		return headers;
	}

	public void setHeaders(List<HeaderConfig> headers) {
		this.headers = headers;
	}

}
