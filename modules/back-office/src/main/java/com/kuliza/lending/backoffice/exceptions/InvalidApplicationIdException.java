package com.kuliza.lending.backoffice.exceptions;

public class InvalidApplicationIdException extends RuntimeException {

	public InvalidApplicationIdException() {
		super();
	}

	public InvalidApplicationIdException(String message) {
		super(message);
	}
}
