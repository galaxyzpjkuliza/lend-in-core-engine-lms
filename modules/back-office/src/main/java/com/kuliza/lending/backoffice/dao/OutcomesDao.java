package com.kuliza.lending.backoffice.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.backoffice.models.Outcomes;
import com.kuliza.lending.backoffice.models.Roles;

@Repository
public interface OutcomesDao extends CrudRepository<Outcomes, Long> {

	public Outcomes findById(long id);

	public Outcomes findByIdAndIsDeleted(long id, boolean isDeleted);

	public List<Outcomes> findByLabel(String label);

	public List<Outcomes> findByLabelAndIsDeleted(String label, boolean isDeleted);

	public Outcomes findByOutcomeKeyAndIsDeleted(String outcomeKey, boolean isDeleted);

	public Outcomes findByOutcomeKeyAndRoleAndIsDeleted(String outcomeKey, Roles role, boolean isDeleted);

}
