package com.kuliza.lending.backoffice.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.backoffice.models.Roles;
import com.kuliza.lending.backoffice.models.Tabs;

@Repository
public interface TabsDao extends CrudRepository<Tabs, Long> {

	public Tabs findById(long id);

	public Tabs findByIdAndIsDeleted(long id, boolean isDeleted);

	public List<Tabs> findByName(String name);

	public List<Tabs> findByNameAndIsDeleted(String name, boolean isDeleted);

	public Tabs findByTabKeyAndIsDeleted(String tabKey, boolean isDeleted);

	public Tabs findByTabKeyAndRoleAndIsDeleted(String tabKey, Roles role, boolean isDeleted);

}
