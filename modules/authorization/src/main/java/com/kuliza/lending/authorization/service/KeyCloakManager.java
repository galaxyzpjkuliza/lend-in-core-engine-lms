package com.kuliza.lending.authorization.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;

import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.keycloak.representations.idm.UserSessionRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.authorization.config.keycloak.KeyCloakService;
import com.kuliza.lending.authorization.exception.ActiveSessionsExceededException;
import com.kuliza.lending.authorization.exception.InternalServerError;
import com.kuliza.lending.authorization.exception.KeyCloakAuthenticationException;
import com.kuliza.lending.authorization.exception.MockException;
import com.kuliza.lending.authorization.exception.RoleDoesNotExistException;
import com.kuliza.lending.authorization.exception.UserNotFoundException;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.NumberUtil;
import com.kuliza.lending.utils.AuthConstants;

@Service
public class KeyCloakManager {

	private KeyCloakService keyCloakService;

	private static final Logger logger = LoggerFactory.getLogger(KeyCloakManager.class);

	@Autowired
	public KeyCloakManager(KeyCloakService keyCloakService) {
		this.keyCloakService = keyCloakService;
	}

	/*****
	 * this method is used to Login with email and password if existing user is
	 * there and checks of active sessions for User If any existing session is there
	 * for user Deleting all the existing sessions and creating new session and
	 * returning
	 * 
	 * @param email
	 * @param password
	 * @return
	 */
	public ApiResponse loginWithEmailAndPassword(String email, String password) {
		logger.info("-->Entering loginWithEmailAndPassword()");
		if (NumberUtil.isNumeric(email))
			throw new MockException(email);
		UserRepresentation userRepresentation = this.keyCloakService.findUserByEmail(email);

		// check of existing sessions of user if there delete them
		List<UserSessionRepresentation> sessions = this.keyCloakService
				.getAllActiveSessionsForUser(userRepresentation.getId());
		int maxActiveSessionsForUser = this.keyCloakService.getActiveSessionsFlagFromConfig();
		int userOfflineSessionCount = this.keyCloakService.getUserOFFlineSession(userRepresentation.getId());
		if (!sessions.isEmpty() && (sessions.size() == maxActiveSessionsForUser)) {
			logger.debug("Active sessions count for user is " + sessions.size());
			if (maxActiveSessionsForUser == 1) {
				logger.debug("Removing all previous active sessions");
				this.keyCloakService.logoutUserFromAllSessions(userRepresentation.getId());
			} else if (maxActiveSessionsForUser >= sessions.size()) {
				throw new ActiveSessionsExceededException(
						"Max number of active sessions per user are allotted. Please logout from other devices and continue");
			}
		}
		if (!(userOfflineSessionCount <= this.keyCloakService.getOfflineSessionsFlagFromConfig())) {
			throw new InternalServerError("Can not create more than " + userOfflineSessionCount + " offline sessions");
		}

		Form form = new Form();
		form.param(AuthConstants.AUTH_GRANT_TYPE, AuthConstants.AUTH_PASSWORD);
		form.param(AuthConstants.AUTH_USER_NAME, userRepresentation.getUsername());
		form.param(AuthConstants.AUTH_PASSWORD, password);

		// uncomment below code if offline token is required
//		form.param("scope", "offline_access");

		logger.debug("userName for emailId is:" + form.asMap().get(AuthConstants.AUTH_USER_NAME));

		AccessTokenResponse accessTokenResponse = keyCloakService.grantAccessToken(form);
		logger.debug("accessTokenResponse " + accessTokenResponse);
		Map<String, Object> data = new HashMap<>();
		data.put(AuthConstants.AUTH_ACCESS_TOKEN, accessTokenResponse.getToken());
		data.put(AuthConstants.AUTH_ACCESS_TOKEN_EXPIRES_IN, accessTokenResponse.getExpiresIn());
		data.put(AuthConstants.AUTH_REFRESH_TOKEN, accessTokenResponse.getRefreshToken());
		data.put(AuthConstants.AUTH_REFRESH_TOKEN_EXPIRES_IN, accessTokenResponse.getRefreshExpiresIn());
		data.put(AuthConstants.AUTH_TOKEN_TYPE, accessTokenResponse.getTokenType());
		data.put(AuthConstants.AUTH_USER_NAME, userRepresentation.getEmail());
		data.put(AuthConstants.AUTH_ROLE, this.keyCloakService.getClientRoleListForUser(userRepresentation.getId()));
		logger.debug(" client roles " + data.get(AuthConstants.AUTH_ROLE));
		logger.info("<-- Exiting loginWithEmailAndPassword()");
		return new ApiResponse(HttpStatus.OK, "SUCCESS", data);
	}

	/**
	 * This method is to get token response and 
	 * other details related to token like expiry time
	 * @param refreshToken
	 * @return 
	 */
	public ApiResponse getAccessTokenByRefreshToken(String refreshToken) {
		Form form = new Form();
		form.param(AuthConstants.AUTH_GRANT_TYPE, AuthConstants.AUTH_REFRESH_TOKEN);
		form.param(AuthConstants.AUTH_REFRESH_TOKEN, refreshToken);
		AccessTokenResponse accessTokenResponse = keyCloakService.grantAccessToken(form);
		String keyCloakUserId = this.keyCloakService
				.extractKeyCloakUserIdFromAccessToken(accessTokenResponse.getToken());
		UserRepresentation userRepresentation = this.keyCloakService.findUserByKeyCloakUserId(keyCloakUserId);
		Map<String, Object> data = new HashMap<>();
		data.put(AuthConstants.AUTH_ACCESS_TOKEN, accessTokenResponse.getToken());
		data.put(AuthConstants.AUTH_ACCESS_TOKEN_EXPIRES_IN, accessTokenResponse.getExpiresIn());
		data.put(AuthConstants.AUTH_REFRESH_TOKEN, accessTokenResponse.getRefreshToken());
		data.put(AuthConstants.AUTH_REFRESH_TOKEN_EXPIRES_IN, accessTokenResponse.getRefreshExpiresIn());
		data.put(AuthConstants.AUTH_TOKEN_TYPE, accessTokenResponse.getTokenType());
		data.put(AuthConstants.AUTH_USER_NAME, userRepresentation.getEmail());
		data.put(AuthConstants.AUTH_ROLE, this.keyCloakService.getClientRoleListForUser(keyCloakUserId));
		return new ApiResponse(HttpStatus.OK, "SUCCESS", data);
	}

	/**
	 * This method is used to Authenticate a request by checking detais in token
	 * @param token
	 * @return keycloak userId
	 */
	public String auth(String token) {
		if (token.isEmpty())
			throw new KeyCloakAuthenticationException(AuthConstants.AUTH_EMPTY_TOKEN_MESSAGE);
		if (this.keyCloakService.isTokenActive(token)) {
			return this.keyCloakService.extractKeyCloakUserIdFromAccessToken(token);
		}
		throw new KeyCloakAuthenticationException(AuthConstants.AUTH_INVALID_TOKEN_MESSAGE);
	}

	/**
	 * this method is used to logout user
	 * @param refresh_token
	 * @return
	 */
	public ApiResponse logout(String refresh_token) {
		ApiResponse apiResponse = new ApiResponse(HttpStatus.OK, "SUCCESS", "Logged Out");
		Form form = new Form();
		form.param(AuthConstants.AUTH_REFRESH_TOKEN, refresh_token);
		Response resp = this.keyCloakService.logoutUser(form);
		return apiResponse;
	}

	/**
	 * This method takes user detalis to create new user
	 * @param firstName
	 * @param lastName 
	 * @param username -mandatory
	 * @param emailId -mandatory
	 * @param password -Mandatory
	 * @param roles
	 * @param attributes
	 * @return details of user if created else error if existing
	 */
	public ApiResponse signup(String firstName, String lastName, String username, String emailId, String password,
			List<String> roles, Map<String, List<String>> attributes) {
		UserRepresentation user = keyCloakService.createUser(firstName, lastName, username, emailId, password, roles,
				attributes);
		return new ApiResponse(HttpStatus.OK, "SUCCESS", user);
	}

	/**
	 * to create a user with role
	 * @param emailId -mandatory
	 * @param password -mandatory
	 * @param roles
	 * @param attributes
	 * @return
	 */
	public ApiResponse createUserWithRole(String emailId, String password, List<String> roles,
			Map<String, List<String>> attributes) {
		logger.info("-->Entering createUserWithRole()");
		UserRepresentation user = keyCloakService.createUserWithRole(emailId, password, roles, attributes);
		return new ApiResponse(HttpStatus.OK, "SUCCESS", user);
	}

	public ApiResponse createUserWithRole(String emailId, String password, String role) {
		return createUserWithRole(emailId, password, Arrays.asList(role), null);
	}
	
	/**
	 * This method is used to update user details
	 * @param userName
	 * @param isEnabled
	 * @param role
	 * @return
	 */
	public ApiResponse updateUserDetails(String userName, Boolean isEnabled, String role) {
		logger.info("--> Entering updateUserDetails()");
		if (isEnabled != null && role != null) {
			keyCloakService.enableFlagUpdate(userName, isEnabled);
			keyCloakService.updateRole(userName, role);
		} else if (isEnabled != null) {
			keyCloakService.enableFlagUpdate(userName, isEnabled);
		} else {
			keyCloakService.updateRole(userName, role);
		}
		logger.info("<--Exiting updateUserDetails()");
		return new ApiResponse(HttpStatus.OK, "SUCCESS", true);
	}

	/**
	 * This method is used to List all the users associated with role
	 * @param roleName
	 * @return list of users
	 */
	public List<String> findUsersByRole(String roleName) {
		logger.info("--> Entering findUsersByRole()");
		Set<UserRepresentation> users = this.keyCloakService.findUsersByRole(roleName);
		List<String> userEmails = new ArrayList<>();
		users.forEach(user -> userEmails.add(user.getEmail()));
		logger.info("<-- Exiting findUsersByRole()");
		return userEmails;
	}

	/**
	 * this method is used to list all the roles for Client
	 * @return list of client level roles
	 */
	public List<String> roleList() {
		List<RoleRepresentation> roleRepresentations = this.keyCloakService.getRoles();
		List<String> roles = new ArrayList<>();
		roleRepresentations.forEach(roleRepresentation -> roles.add(roleRepresentation.getName()));
		return roles;
	}

	/**
	 * This method is used to List all the users in realm
	 * @return list of usernames
	 */
	public List<Map<String, Object>> getUsersList() {
		List<Map<String, Object>> data = this.keyCloakService.getUsers();
		return data;
	}

	/**
	 * Create a role in realm
	 * @param role
	 */
	public void createRole(String role) {
		this.keyCloakService.createRole(role);
	}

	/**
	 * This method is used to List of roles associated with user
	 * @param emailId
	 * @return list user roles
	 */
	public List<String> findRoleByEmailId(String emailId) {
		return this.keyCloakService.getClientRoleListForUser(this.keyCloakService.findUserByEmail(emailId).getId());
	}

	/**
	 * This method is used to create new roles in realm
	 * @param roles
	 * @return List of roles with status created or already existing
	 */
	public Map<String, String> createNewRole(List<String> roles) {
		logger.info("--> Entering createNewRole()");
		Map<String, String> rolesMap = new HashMap<>();
		for (String role : roles) {
			logger.debug("role" + role);
			if (!this.keyCloakService.checkForExistingRole(role)) {
				this.keyCloakService.createRole(role);
				rolesMap.put(role, AuthConstants.AUTH_CREATED_SUCCESSFULLY);
			} else {
				rolesMap.put(role, AuthConstants.AUTH_ALREADY_EXISTING);
			}

		}
		logger.info("<--Exiting createNewRole()");
		return rolesMap;

	}

	/**
	 * This method is used to add the list of roles to user
	 * @param emailID
	 * @param roles
	 * @return 
	 */
	public String addUserToRole(String emailID, List<String> roles) {
		logger.info("--> Entering addUserToRole()");
		// search user with email
		logger.debug("Searching user with email :-" + emailID);
		UserRepresentation rep = this.keyCloakService.findUserByEmail(emailID);
		if (rep != null && rep.getEmail().equals(emailID)) {
			// search role
			logger.debug("user with email id found checking for role");
			for (String role : roles) {
				if (this.keyCloakService.checkForExistingRole(role)) {
					logger.info("found existing role");
					this.keyCloakService.addUserToRole(rep.getId(), role);
				} else {
					logger.error("No role exists :-" + role);
					throw new RoleDoesNotExistException(
							"No role exists. Please make sure role [" + role + "] is already created ");
				}
			}
		} else {
			logger.error("No user exists wiht given emailID " + emailID);
			throw new UserNotFoundException("User with email not found " + emailID);
		}

		return "Roles ::" + roles.toString() + " successfully assigned  to user";
	}

	/**
	 * This method is used to check for existing user
	 * @param user
	 * @return
	 */
	public boolean checkForExistingUser(String user) {
		return this.keyCloakService.checkForExistingUser(user);
	}

	/**
	 * This method is used to create new groups in Realm
	 * @param groups
	 * @return list of groups with their status "Created " or "Existing"
	 */
	public Map<String, String> createGroups(List<String> groups) {
		logger.info("--> Entering createGroups()");
		Map<String, String> result = new HashMap<>();
		for (String group : groups) {
			if (!this.keyCloakService.checkForExistingGroup(group)) {
				logger.debug("Creating new group with name " + group);
				this.keyCloakService.createNewGroup(group);
				result.put(group, AuthConstants.AUTH_CREATED_SUCCESSFULLY);
			} else {
				logger.error("found existing group with name :-" + group);
				result.put(group, AuthConstants.AUTH_ALREADY_EXISTING);
			}
		}
		logger.info("<-- Exiting createGroups()");
		return result;
	}
}
