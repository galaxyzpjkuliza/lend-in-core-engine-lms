package com.kuliza.lending.authorization.config.keycloak;

import static javax.servlet.http.HttpServletResponse.SC_FORBIDDEN;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.kuliza.lending.common.pojo.ApiResponse;

@Component
public class KeyCloakAuthenticationEntryPoint implements AuthenticationEntryPoint {

	@Autowired
	@Qualifier("mappingJackson2HttpMessageConverter")
	HttpMessageConverter httpMessageConverter;

	@Override
	public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			AuthenticationException e) throws IOException, ServletException {
		httpServletResponse.setStatus(SC_FORBIDDEN);
		httpServletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);
		String message;
		if (e.getCause() != null) {
			message = e.getCause().getMessage();

		} else {
			message = e.getMessage();
		}

		ApiResponse apiResponse = new ApiResponse(HttpStatus.UNAUTHORIZED, message, null);
		ServerHttpResponse outputMessage = new ServletServerHttpResponse(httpServletResponse);
		outputMessage.setStatusCode(HttpStatus.OK);
		httpMessageConverter.write(apiResponse, null, outputMessage);
	}
}
